## 窗口对象

存储窗口的句柄与位置信息, 用于`BaseGame`对象的初始化

```python
from autogame.base import Window

window = Window("XX模拟器")
```
    
**属性**

- *handle* - 窗口的句柄
- *left* - 窗口左边框位置
- *top* - 窗口上边框位置
- *right* - 窗口右边框位置
- *bottom* - 窗口下边框位置

## 获取窗口标题

防止无法直接获得窗口标题或标题不准确, 本模块提供以下函数用于获取所有窗口的标题

```python
from autogame.base import _all_windows

names = _all_windows()
```
