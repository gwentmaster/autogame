# BaseGame类

`autogame.base.BaseGame(window_name, pic_path="./pic", suffix=set(), threshold=0.5)`

基本的自动点击类, 已实现识图、鼠标点击等基本方法

**params**

- *window_name* - 窗口的名称, 据此获取窗口句柄与位置信息
- *pic_path* - 模板图片所在路径
- *suffix* - `pic_path`下会被识别成模板图片的后缀名, 默认已包含jpg、png、gif、bmp、jpeg
- *threshold* - 识图片的阈值, 匹配度超过此值才认为找到了图片


## methods

### capture_window

`def capture_window(self) -> numpy.ndarray`

获取窗口截图, 用于在其中寻找模板图片
返回一个numpy的数组对象

### find_pic

`def find_pic(self, pic: str) -> Optional[Tuple[int, int]]`

在当前窗口中查找模板图片, 未找到返回`None`, 找到返回模板图片中心点相对窗口的坐标

> 还未实现根据窗口大小调整图片分辨率

### click

`def click(self, pos: Tuple[int, int]) -> None`

鼠标点击指定位置

**params**

- *pos* - 需点击的位置, 相对窗口而言, 格式为(width, height)

### run_task

`def run_task(self, task: AutogameTask) -> None`

运行指定任务

**params**

- *task* - `autogame.task.AutogameTask`对象
