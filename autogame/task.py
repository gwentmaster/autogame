#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-06-23 15:20:41
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


import json
from collections import namedtuple
from typing import cast, Dict, List, Union

from autogame.exceptions import TaskFormatExceptiion


SingleTaskDic = Dict[str, Union[str, List]]
TaskDic = Dict[str, Union[str, SingleTaskDic, List[SingleTaskDic]]]


SingleTask = namedtuple("SingleTask", ["method", "args"])


class AutoGameTask(object):

    def __init__(self, data: Union[str, TaskDic]):  # TODO 添加self.game属性标志所属游戏
        self.game = None
        if isinstance(data, str):
            self.loads(data)
        elif isinstance(data, dict):
            self.load(data)

    def load(self, task_dic: TaskDic) -> "AutoGameTask":
        """summary

        Args:
            task_dic:
                {"name": "some_name",
                 "if": {"method": "some_method", "args": ["some_args"]},
                 "else": [{"method": "some_method", "args": ["some_args"]}]
                 "tasks": [{"method": "some_method", "args": ["some_args"]}]}

        Returns:
            description
            type
        """

        try:
            self.name = cast(str, task_dic["name"])

            if_dic = cast(SingleTaskDic, task_dic["if"])
            self.if_condition = SingleTask(
                method=if_dic["method"],
                args=if_dic["args"]
            )

            self.else_tasks = []  # type: List[SingleTask]
            for task in task_dic["else"]:
                task = cast(SingleTaskDic, task)
                self.else_tasks.append(
                    SingleTask(method=task["method"], args=task["args"])
                )

            self.tasks = []
            for task in task_dic["tasks"]:
                task = cast(SingleTaskDic, task)
                self.tasks.append(
                    SingleTask(method=task["method"], args=task["args"])
                )

        except KeyError:
            raise TaskFormatExceptiion()
        return self

    def loads(self, task_string: str) -> "AutoGameTask":

        task_dic = json.loads(task_string)
        self.load(task_dic)
        return self

    def dump(self) -> TaskDic:

        result = {}  # type: TaskDic

        result["name"] = self.name

        if_condition = {}  # type: SingleTaskDic
        if_condition["method"] = self.if_condition.method
        if_condition["args"] = self.if_condition.args
        result["if"] = if_condition

        else_tasks = []  # type: List[SingleTaskDic]
        for t in self.else_tasks:
            else_tasks.append({"method": t.method, "args": t.args})
        result["else"] = else_tasks

        tasks = []  # type: List[SingleTaskDic]
        for t in self.tasks:
            tasks.append({"method": t.method, "args": t.args})
        result["tasks"] = tasks

        return result

    def dumps(self) -> str:

        return json.dumps(
            self.dump(),
            ensure_ascii=False,
            separators=(",", ";")
        )
