#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-06-15 22:25:52
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


class AutoGameBaseException(Exception):
    """基本错误类
    """
    pass


class WindowNotFoundException(AutoGameBaseException):
    """未找到指定窗口
    """
    pass


class PicDirNotFoundException(AutoGameBaseException):
    """所提供的存放图片文件的路径不存在
    """
    pass


class PicNotFoundException(AutoGameBaseException):
    """指定图片不存在
    """
    pass


class ClickException(AutoGameBaseException):
    """点击错误
    """
    pass


class StopTaskException(AutoGameBaseException):
    """用于停止任务
    """
    pass


class TaskFormatExceptiion(AutoGameBaseException):
    """任务格式错误
    """
    pass
