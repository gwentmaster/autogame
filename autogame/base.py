#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-06-15 22:15:29
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


import logging
import os
import time
from pathlib import Path
from typing import (
    cast,
    Dict,
    Iterable,
    Optional,
    Tuple,
    Union
)

import cv2
import win32api
import win32con
import win32gui
import win32ui
import numpy as np
from PIL import Image
from pynput.mouse import Button, Controller

from autogame.task import AutoGameTask, SingleTask, TaskDic
from autogame.exceptions import (
    ClickException,
    PicDirNotFoundException,
    PicNotFoundException,
    StopTaskException,
    WindowNotFoundException
)


logger = logging.getLogger("autogame")


def _all_windows():
    """获取窗口标题,用于初始化Window对象

    Returns:
        窗口标题的列表
    """

    names = []

    def get_names(hwnd, mouse):
        if (
                win32gui.IsWindow(hwnd)
                and win32gui.IsWindowEnabled(hwnd)
                and win32gui.IsWindowVisible(hwnd)
        ):
            names.append(win32gui.GetWindowText(hwnd))

    win32gui.EnumWindows(get_names, 0)
    names = [n for n in names if n != ""]
    return names


class Window(object):
    """窗口对象

    Args:
        name: 窗口名称
    """

    def __init__(self, name: str = None):

        if name is None:
            self.handle = cast(int, None)
            self.left = cast(int, None)
            self.top = cast(int, None)
            self.right = cast(int, None)
            self.bottom = cast(int, None)
        else:
            self.load(name)

    def load(self, name: str) -> None:

        handle = win32gui.FindWindow(None, name)
        if not handle:
            raise WindowNotFoundException()

        self.handle = handle

        rect = win32gui.GetWindowRect(handle)
        self.left = rect[0]
        self.top = rect[1]
        self.right = rect[2]
        self.bottom = rect[3]

    def __getattribute__(self, attr: str):

        value = super(Window, self).__getattribute__(attr)
        if value is None:
            raise WindowNotFoundException("call the `load` method first")
        return value


class BaseGame(object):
    """基本自动点击对象

    Args:
        pic_path: 存放图片文件的目录
        window_name: 需要进行自动点击的窗口名称
    """

    def __init__(
        self,
        window_name: str = None,
        pic_path: Union[str, Path] = "./pic",
        suffix: Iterable[str] = None,
        threshold: float = 0.5
    ):
        """summary

        Args:
            window_name: 窗口名称,可不指定,之后通过`load_window`指定即可
            pic_path: 图片路径 (默认为: "./pic")
            suffix: 图片后缀,默认含jpg,png,gif,bmp,jpeg,若指定则会在此基础上更新
            threshold: 图片查找阈值 (默认为: 0.5)

        Raises:
            PicDirNotFoundException: 图片路径无效
        """

        self.window = Window(window_name)
        self.threshold = threshold
        self.mouse = Controller()
        self.suffix = {"jpg", "png", "gif", "bmp", "jpeg"}
        self.suffix.update(suffix or [])
        self.pic_path = pic_path
        self.picture = {}  # type: Dict[str, Union[str, Path]]
        self.cancle_signal = False  # 是否收到停止信号

        self.load_pic(self.pic_path)

        self._click = self.pywin32_click  # 默认使用pywin32点击

    def load_pic(self, pic_path: Union[str, Path] = None) -> None:
        """从图片路径查找图片并记录名称,也可用于路径下增减图片后重新载入

        Args:
            pic_path: 图片所在目录

        Raises:
            PicDirNotFoundException: 图片路径不存在
        """

        pic_path = self.pic_path if pic_path is None else pic_path

        if not os.path.isdir(pic_path):
            logger.error(f"图片路径不存在,请重新指定: {pic_path}")
            raise PicDirNotFoundException(pic_path)

        for file in os.listdir(pic_path):
            pic = Path(pic_path) / Path(file)
            if pic.suffix[1:] not in self.suffix:
                continue
            self.picture[pic.name.rstrip(pic.suffix)] = pic

    def load_window(self, name: str) -> None:
        """加载窗口,用于更换控制的窗口

        Args:
            name: 窗口名称
        """

        self.window.load(name)

    def set_foreground(self) -> None:
        """将当前控制的窗口调至前台
        """

        win32gui.SetForegroundWindow(self.window.handle)

    def capture_window(self) -> np.ndarray:
        """
        根据窗口句柄获取窗口截图,目标窗口可位于后台,但不能最小化

        Args:
            handle: 目标窗口的句柄

        Returns:
            目标窗口的截图
        """
        left, top, right, bot = win32gui.GetWindowRect(self.window.handle)
        w = right - left
        h = bot - top
        # 返回句柄窗口的设备环境、覆盖整个窗口，包括非客户区，标题栏，菜单，边框
        hwndDC = win32gui.GetWindowDC(self.window.handle)
        # 创建设备描述表
        mfcDC = win32ui.CreateDCFromHandle(hwndDC)
        # 创建内存设备描述表
        saveDC = mfcDC.CreateCompatibleDC()
        # 创建位图对象
        saveBitMap = win32ui.CreateBitmap()
        saveBitMap.CreateCompatibleBitmap(mfcDC, w, h)
        saveDC.SelectObject(saveBitMap)
        # # 截图至内存设备描述表
        img_dc = mfcDC
        mem_dc = saveDC
        mem_dc.BitBlt((0, 0), (w, h), img_dc, (0, 0), win32con.SRCCOPY)
        bmpinfo = saveBitMap.GetInfo()
        bmpstr = saveBitMap.GetBitmapBits(True)
        # 生成图像
        im = Image.frombuffer(
            'RGB',
            (bmpinfo['bmWidth'], bmpinfo['bmHeight']),
            bmpstr, 'raw', 'BGRX', 0, 1
        )
        return np.array(im)

    def find(
        self,
        pic: str,
        threshold: float = None
    ) -> Optional[Tuple[int, int]]:
        """查找图片

        Args:
            pic: 图片名称
            threshold: 查找阈值

        Returns:
            相对窗口而言的图片中心点坐标
        """

        if threshold is None:
            threshold = self.threshold

        if pic not in self.picture:
            logger.error(f"图片不存在: [{pic}]")
            raise PicNotFoundException(pic)

        window = self.capture_window()
        template = np.array(Image.open(self.picture[pic]))
        temp_height, temp_width, _ = template.shape

        window_bgr = cv2.split(window)
        template_bgr = cv2.split(template)
        weight = (0.3, 0.3, 0.4)
        resbgr = {}
        for i in range(3):
            resbgr[i] = cv2.matchTemplate(
                window_bgr[i],
                template_bgr[i],
                cv2.TM_CCOEFF_NORMED
            )
        res = sum(resbgr[i]*weight[i] for i in range(3))

        _, max_val, _, left_top = cv2.minMaxLoc(res)

        if max_val < threshold:
            logger.info(f"未找到图片: [{pic}], 当前阈值: {threshold}")
            return None

        logger.info(f"找到图片: [{pic}]")
        if logger.level <= logging.DEBUG:
            window_img = Image.fromarray(window)
            result_img = window_img.crop((
                left_top[0],
                left_top[1],
                left_top[0] + temp_width,
                left_top[1] + temp_height
            ))
            logger.debug("", extra={"image": result_img})
        return (left_top[0] + round(temp_width/2),
                left_top[1] + round(temp_height/2))

    def pynput_click(self, pos: Tuple[int, int]) -> None:
        """pynput鼠标点击,只能前台操作

        Args:
            pos: 需要点击的位置,相对窗口而言,格式为(width, height)
        """

        self.mouse.position = (self.window.left + pos[0],
                               self.window.top + pos[1])
        self.mouse.click(Button.left, 1)

    def click(self, before: str, after: str = None) -> None:
        """
        """

        logger.info(f"开始点击: [{before}]")

        count = 0
        if after is None:
            while count < 10:
                self._judge()
                count += 1
                pos = self.find(before)
                if pos:
                    self._click(pos)
                    time.sleep(1)
                    return None
        elif after == "not_find":
            while True:
                self._judge()
                count += 1
                pos = self.find(before)
                if pos:
                    self._click(pos)
                    time.sleep(1)
                else:
                    return None
                if count >= 10:
                    break
        else:
            while not self.find(after):
                self._judge()
                count += 1
                if count >= 11:
                    break
                pos = self.find(before)
                if pos:
                    self._click(pos)
                time.sleep(1)
            else:
                return None
        logger.error(f"10次尝试点击失败: [{before}]")

    def pywin32_click(self, pos: Tuple[int, int]) -> None:
        """pywin32鼠标点击,在有管理员权限的情况下进行后台鼠标点击

        Args:
            pos: 需要点击的位置,以窗口左上角为坐标原点
        """

        long_pos = win32api.MAKELONG(pos[0], pos[1])

        win32api.PostMessage(self.window.handle,
                             win32con.WM_LBUTTONDOWN,
                             win32con.MK_LBUTTON,
                             long_pos)
        win32api.PostMessage(self.window.handle,
                             win32con.WM_LBUTTONUP,
                             win32con.MK_LBUTTON,
                             long_pos)

    def switch_click_method(self, method: str) -> None:
        """更换点击方式

        Args:
            method: 可选择"pywin32"或"pynput"
        """

        if method == "pywin32":
            self._click = self.pywin32_click
        elif method == "pynput":
            self._click = self.pynput_click

    def drag(
        self,
        pos: Tuple[Union[int, float], Union[int, float]],
        orientation: int,
        towards: int,
        span: int,
        hold_for: int = 1
    ) -> None:
        """pywin32鼠标拖动

        Args:
            pos: 起始位置,以窗口左上为坐标原点,结构如(x, y),
                 x为坐标点距窗口左边界的距离,y为坐标点距窗口上边界的距离
                 若x,y均小于1,则认为起始点坐标为(x*窗口宽度, y*窗口高度)
            orientation: 方向,0为水平,1为竖直
            towards: 鼠标移动方向,1为左至右或上至下,-1反之
            span: 跨度,像素值
            hold_for: 鼠标移动至目标点后直到松开左键所经过的秒数,默认1s
        """

        # 起始位置计算
        ori_x, ori_y = pos
        if (0 < ori_x < 1) and (0 < ori_y < 1):
            ori_x = int(ori_x * (self.window.right - self.window.left))
            ori_y = int(ori_y * (self.window.bottom - self.window.top))
        else:
            ori_x, ori_y = int(ori_x), int(ori_y)

        # 左键按下
        win32gui.PostMessage(
            self.window.handle,
            win32con.WM_LBUTTONDOWN,
            win32con.MK_LBUTTON,
            win32api.MAKELONG(ori_x, ori_y)
        )

        # 鼠标移动
        window_width = self.window.right - self.window.left
        window_height = self.window.bottom - self.window.top
        des_x, des_y = ori_x, ori_y
        for i in range(span):
            time.sleep(0.01)  # 需要有一定间隔,不然无法拖动
            des_x += towards*(1 - orientation)
            des_y += towards*orientation
            if (
                    (des_x < 0) or (des_x > window_width)
                    or (des_y < 0) or (des_y > window_height)
            ):
                des_x = max(des_x, 0)
                des_x = min(des_x, window_width)
                des_y = max(des_y, 0)
                des_y = min(des_y, window_height)
                break
            win32gui.PostMessage(
                self.window.handle,
                win32con.WM_MOUSEMOVE,
                win32con.MK_LBUTTON,
                win32api.MAKELONG(des_x, des_y)
            )

        # 按住指定时间后释放鼠标左键
        time.sleep(hold_for)
        win32gui.PostMessage(
            self.window.handle,
            win32con.WM_LBUTTONUP,
            des_x, des_y
        )

    def sleep(self, second: int) -> None:
        """休眠,每秒会检查任务是否中止

        Args:
            second: 休眠秒数

        Raises:
            StopTaskException: 通过抛出此错误来中止任务
        """

        logger.info(f"开始等待: [{second}] 秒")
        for _ in range(second):
            self._judge()
            time.sleep(1)

    def cancle(self):
        """取消任务
        """

        self.cancle_signal = True

    def _judge(self):
        """判断是否该中止任务
        """

        if self.cancle_signal:
            logger.info("任务中止")
            raise StopTaskException()

    def little_loop(self, task_dic: TaskDic) -> None:
        """创建并运行子任务

        Args:
            task_dic: 任务数据
        """

        task = AutoGameTask(data=task_dic)
        self.run_task(task)

    def run_task(self, task: AutoGameTask) -> None:
        """运行任务

        Args:
            task: 自动点击任务

        Raises:
            StopTaskException: 通过抛出此错误来中止任务
        """

        self.cancle_signal = False

        self.set_foreground()

        # 防止程序无法退出
        if task.else_tasks == []:
            task.else_tasks.append(SingleTask(method="sleep", args=[1]))

        while True:

            if getattr(self, task.if_condition.method)(
                *task.if_condition.args
            ):
                for t in task.tasks:

                    self._judge()
                    getattr(self, t.method)(*t.args)

            else:
                for t in task.else_tasks:
                    self._judge()
                    getattr(self, t.method)(*t.args)
