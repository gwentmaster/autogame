#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-09-22 14:37:05
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


from typing import Any, Dict


class Config(object):
    """
    配置类

    使用类似"section.sub_section.option.sub_option"以点号分隔的键来取值,
    设置值时不存在的节点会自动填充

    Args:
        config: 初始配置的字典
    """

    def __init__(self, config: Dict):

        self.config = config

    def get(self, key: str, default: Any = None):

        try:
            value = self.__getitem__(key)
        except KeyError:
            value = default
        return value

    def __getitem__(self, key: str):

        target = self.config
        for k in key.split("."):
            try:
                target = target[k]
            except KeyError:
                break
        else:
            return target

        raise KeyError(key)

    def __setitem__(self, key: str, value: Any):

        keys = key.split(".")
        target = self.config
        for k in keys[:-1]:
            target = target.setdefault(k, {})
        target[keys[-1]] = value
