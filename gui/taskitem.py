#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-08-01 07:44:13
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


"""
任务区中显示的任务
"""

from PyQt5.QtCore import (
    QObject,
    QSize,
    Qt
)
from PyQt5.QtGui import QCursor, QIcon
from PyQt5.QtWidgets import (
    QHBoxLayout,
    QLabel,
    QPushButton,
    QWidget
)

# TODO
import sys
sys.path.insert(0, "../")
# TODO
from autogame.base import BaseGame
from autogame.task import AutoGameTask, TaskDic


class TaskItem(QWidget):
    """任务项,放置于QListView中

    Args:
        task: 此项对应的任务
        parent: 此项的父组件

    Attributes:
        task: 包含的任务
        label: 含任务名称的标签
        run: 运行按钮
        edit: 编辑按钮
        delete: 删除按钮
    """

    def __init__(
        self,
        task: AutoGameTask,
        parent: QObject = None,
        *args,
        **kwargs
    ):

        super(TaskItem, self).__init__(parent, *args, **kwargs)
        self.task = task
        self.label = QLabel(self.task.name, self)

        # 运行任务按钮
        self.run = QPushButton(self)
        self.run.setIcon(QIcon("./common/application/run.jpg"))
        self.run.setIconSize(QSize(30, 30))
        self.run.setCursor(QCursor(Qt.PointingHandCursor))

        # 编辑任务按钮
        self.edit = QPushButton(self)
        self.edit.setIcon(QIcon("./common/application/edit.jpg"))
        self.edit.setIconSize(QSize(30, 30))
        self.edit.setCursor(QCursor(Qt.PointingHandCursor))

        # 删除任务按钮
        self.delete = QPushButton(self)
        self.delete.setIcon(QIcon("./common/application/delete.jpg"))
        self.delete.setIconSize(QSize(30, 30))
        self.delete.setCursor(QCursor(Qt.PointingHandCursor))

        # 布局
        self.layout = QHBoxLayout(self)
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.run)
        self.layout.addWidget(self.edit)
        self.layout.addWidget(self.delete)
