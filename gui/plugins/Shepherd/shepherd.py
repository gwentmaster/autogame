#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-09-01 20:49:14
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


import json
import logging
import os
from collections import defaultdict
from itertools import chain
from typing import Callable, Dict, List, Mapping, Tuple

# TODO
import sys
sys.path.insert(0, "../../../")
# TODO
from autogame.base import BaseGame
from autogame.config import Config
from .tasks import TASKS


LOGGER = logging.getLogger("autogame")  # type: logging.Logger


def load_config() -> Dict:

    for name in ("User.autogame-config", "Default.autogame-config"):
        try:
            config_file = open(name, "r", encoding="utf-8")
        except FileNotFoundError:
            continue
        else:
            break
    else:
        return {}

    try:
        config = json.load(config_file)
    except json.JSONDecodeError:
        LOGGER.error(f"配置文件格式错误: {os.getcwd()}\\{config_file.name}")
        return {}
    finally:
        config_file.close()
    return config


def load_description() -> Dict:

    try:
        description_file = open(
            "Default.autogame-config",
            "r",
            encoding="utf-8"
        )
        description = json.load(description_file)
        description_file.close()
        return description
    except FileNotFoundError:
        return {}
    except json.JSONDecodeError:
        description_file.close()
        return {}


class ShepherdGame(BaseGame):

    _gamename_ = "牧羊人之心"
    _config_ = Config(load_config())  # type: Config
    _description_ = Config(load_description())  # type: Config

    def __init__(self, *args, **kwargs):

        super(ShepherdGame, self).__init__(*args, **kwargs)

        self.go_functions = {
            "llts": self._go_llts,
            "lah": self._go_lah,
            "bslm": self._go_bslm,
            "nslm": self._go_nslm,
            "lrx": self._go_lrx
        }  # type: Mapping[str, Callable]

        self.task_functions = {
            "": None
        }  # type: Mapping[str, Callable]

        # 可以接取的任务,地名为键,任务名称的列表为值
        self.tasks = defaultdict(list)  # type: Dict[str, List[str]]

        # 已接取但未进行的任务
        self.accepted_tasks = None  # type: List[str]

        for path in self._config_.get("pic_paths", []):
            self.load_pic(pic_path=path)

    def return_home(self) -> None:
        """返回魔物牧场界面
        """

        while True:

            if self.find("培育室", self.threshold):
                LOGGER.info("已回到主界面")
                break

            for pic in ("农场", "农场(城镇)", "回程", "返回", "返回(小)", "离开",
                        "战斗胜利", "获得物品", "跳过剧情", "触摸继续"):
                if self.find(pic, self.threshold):
                    self.click(pic)
                    break
            else:
                self.sleep(2)

    def collect_task_reward(self) -> None:
        """收集任务奖励
        """

        self.return_home()
        self.click()

    """===========================地图跳转方法==========================="""

    def _go_llts(self) -> None:

        while True:

            self._judge()

            if not self.find("地图-阿格莱恩", self.threshold):
                self.return_home()
                self.click(before="出门", after="探险")
                self.click(before="大地图", after="地图-阿格莱恩")

            if self.find("地图-莉莉托斯", self.threshold):
                break

            self.drag(
                pos=(0.5, 0.5),
                orientation=1,
                towards=1,
                span=400,
                hold_for=1
            )
            self.drag(
                pos=(0.5, 0.5),
                orientation=0,
                towards=-1,
                span=100,
                hold_for=1
            )

        self.click(before="地图-莉莉托斯", after="设施-莉莉托斯")

    def _go_lah(self) -> None:

        while True:

            self._judge()

            if not self.find(pic="地图-阿格莱恩", threshold=self.threshold):
                self.return_home()
                self.click(before="出门", after="探险")
                self.click(before="大地图", after="地图-阿格莱恩")

            if self.find("地图-莉安湖", self.threshold):
                break

            self.drag(
                pos=(0.5, 0.5),
                orientation=1,
                towards=1,
                span=400,
                hold_for=1
            )
            self.drag(
                pos=(0.5, 0.5),
                orientation=0,
                towards=-1,
                span=100,
                hold_for=1
            )

        self.click(before="地图-莉安湖", after="设施-莉安湖")

    def _go_bslm(self) -> None:

        while True:

            self._judge()

            if not self.find(pic="地图-阿格莱恩", threshold=self.threshold):
                self.return_home()
                self.click(before="出门", after="探险")
                self.click(before="大地图", after="地图-阿格莱恩")

            if self.find("地图-北赛拉姆", self.threshold):
                break

            self.drag(
                pos=(0.5, 0.5),
                orientation=1,
                towards=1,
                span=400,
                hold_for=1
            )
            self.drag(
                pos=(0.5, 0.5),
                orientation=0,
                towards=1,
                span=100,
                hold_for=1
            )

        self.click(before="地图-北赛拉姆", after="设施-北赛拉姆")

    def _go_nslm(self) -> None:

        while True:

            self._judge()

            if not self.find(pic="地图-阿格莱恩", threshold=self.threshold):
                self.return_home()
                self.click(before="出门", after="探险")
                self.click(before="大地图", after="地图-阿格莱恩")

            if self.find("地图-南赛拉姆", self.threshold):
                break

            self.drag(
                pos=(0.5, 0.5),
                orientation=1,
                towards=-1,
                span=400,
                hold_for=1
            )
            self.drag(
                pos=(0.5, 0.5),
                orientation=0,
                towards=-1,
                span=100,
                hold_for=1
            )

        self.click(before="地图-南赛拉姆", after="设施-南赛拉姆")

    def _go_lrx(self) -> None:

        while True:

            self._judge()

            if not self.find(pic="地图-阿格莱恩", threshold=self.threshold):
                self.return_home()
                self.click(before="出门", after="探险")
                self.click(before="大地图", after="地图-阿格莱恩")

            if self.find("地图-浪人岬", self.threshold):
                break

            self.drag(
                pos=(0.5, 0.5),
                orientation=1,
                towards=-1,
                span=400,
                hold_for=1
            )
            self.drag(
                pos=(0.5, 0.5),
                orientation=0,
                towards=-1,
                span=100,
                hold_for=1
            )

        self.click(before="地图-浪人岬", after="设施-浪人岬")

    """==========================任务及奖励收取==========================="""

    def task_acception(self) -> None:

        for town in ("llts", "lah", "bslm", "nslm", "lrx"):
            self.go_functions["town"]()
            self.click(before="设施-协会", after="委托")
            self.click(before="委托", after="委托中心")
            for task in TASKS:
                if self.find(task, self.threshold):
                    self.tasks[town].append(task)

        for task in sorted(
                chain(*self.tasks.values()),
                key=lambda x: self._config_.get(f"task_priority.{x[1]}", 4)
        )[:6]:
            self.accepted_tasks.append(task)

    def task_reward_collect(self) -> None:

        pass

    """============================任务执行============================"""


# if __name__ == "__main__":

    # from autogame.base import _all_windows
    # print(_all_windows())
    # game = ShepherdGame(window_name="牧羊人之心 - MuMu模拟器" ,pic_path=r"D:\programs\z\Sublime Text\Code\MyScripts\autogame\gui\common\牧羊人之心")
    # game.return_home()
