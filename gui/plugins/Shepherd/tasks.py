#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-09-09 19:56:29
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


"""各图协会任务"""


TASKS = {
    "llts": ["渣渣布林入侵", "招募门卫"],
    "lah": ["讨伐食人花", "处理毒桶", "温泉治疗"],
    "bslm": ["游乐场扩建"],
    "nslm": ["收购一些石块"],
    "lrx": ["换换口味", "护航小队"]
}
