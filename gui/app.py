#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-07-01 16:14:11
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


import cgitb
import importlib
import json
import logging
import os
import re
import shutil
import sys
from collections import defaultdict, OrderedDict
from configparser import ConfigParser
from ctypes import CDLL
from itertools import chain
from typing import Callable, cast, Dict, Optional

from PIL import ImageGrab
from PyQt5.QtCore import QObject, QSize
from PyQt5.QtGui import (
    QPixmap,
    QStandardItem,
    QStandardItemModel,
    QTextCursor
)
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QMessageBox
)

from gui import UiMainWindow
from log import RollingHandler
from taskitem import TaskItem
from thread import ListenKeyboardThread, TaskThread
# TODO
sys.path.insert(0, "../")
# TODO
from autogame.base import _all_windows, BaseGame
from autogame.exceptions import StopTaskException
from autogame.task import AutoGameTask, TaskDic

# display=0, logdir="./"
cgitb.enable()


class App(QObject):
    """Application

    Attributes:
        auto_game: {BaseGame} - 游戏类,用于执行任务
        capture_dll: {CDLL} - 用于截图的dll
        current_game: {str} - 当前游戏名称
        current_item: {TaskItem} - 当前正在运行的任务对应的任务项
        current_task: {AutoGameTask} - 当前正在运行的任务
        running: {bool} - 是否正在运行任务
        window_name: {str} - 当前正在控制的窗口的名称
    """

    def __init__(self, *args, **kwargs):

        # 清除之前运行的cache文件
        try:
            shutil.rmtree("./cache", ignore_errors=True)
            os.mkdir("./cache")
        except (FileExistsError):
            pass

        super().__init__(*args, **kwargs)
        self.app = QApplication(sys.argv)
        self.main_window = QMainWindow()
        self.ui = UiMainWindow()
        self.ui.setupUi(self.main_window)

        # 运行任务的子线程
        self.task_thread = TaskThread(self.main_window)

        # 监听键盘的子线程
        self.listen_keyboard_thread = ListenKeyboardThread(self.main_window)
        self.listen_keyboard_thread.start()

        # 日志处理
        self.logger = logging.getLogger("autogame")
        self.logger.setLevel(logging.INFO)
        self.log_handler = RollingHandler(
            max_size=50,
            signal=self.task_thread.log_signal
        )
        self.logger.addHandler(self.log_handler)

        # 读取配置文件
        config = ConfigParser()
        config.read("autogame.ini", encoding="utf8")
        self.game_pic: Dict[str, str] = defaultdict(
            lambda: "./common/application/games/default.jpg"
        )
        for option in config.options("games"):
            self.game_pic[option] = config.get("games", option)

        # 读取QSS文件
        with open(
            "./common/application/ui.qss",
            "r",
            encoding="utf-8"
        ) as ui_file:
            self.main_window.setStyleSheet(ui_file.read())

        # 后面需要使用的属性的定义
        self.auto_game: BaseGame = None  # 执行任务的类
        self.capture_dll = CDLL("./common/application/ScreenShot.dll")
        self.current_game = None  # 当前游戏的名称
        self.current_item: TaskItem = None  # 当前正在运行的任务对应的任务项
        self.current_task: AutoGameTask = None  # 当前正在运行的任务
        self.game_factory: Dict[str, type] = (
            defaultdict(lambda: BaseGame)
        )  # 配合插件生成对应游戏类
        self.running = False  # 是否正在运行自动点击任务
        self.window_name = None  # 当前控制的窗口的名称

        self.init_plugin()
        self.init_model()
        self.init_signal()
        self.init_game_choose_box()
        self.init_game_and_window()

    def init_signal(self) -> None:

        # 在游戏下拉框选择游戏后
        self.ui.game_choose.textActivated.connect(self.display_game_pic)

        # 窗口选择界面
        self.ui.window_choose.clicked.connect(self.choose_window)
        self.ui.all_window_return.clicked.connect(self.return_home)
        self.ui.all_window_confirm.clicked.connect(self.set_window)

        # 截图界面
        self.ui.capture_screen.clicked.connect(self.print_screen)
        self.ui.capture_again.clicked.connect(self.print_screen)
        self.ui.save_img.clicked.connect(self.save_img)

        # 任务编辑界面
        self.ui.create_task.clicked.connect(self.task_new)
        self.ui.edit_task_return.clicked.connect(self.return_home)
        self.ui.edit_task_clear.clicked.connect(self.task_editor_clear)
        self.ui.edit_task_confirm.clicked.connect(self.task_save)

        # 切换点击方法
        self.ui.pywin32_chosen.triggered.connect(
            self.switch_click_method("pywin32")
        )
        self.ui.pynput_chosen.triggered.connect(
            self.switch_click_method("pynput")
        )

        # 切换日志debug模式
        self.ui.log_level_menu.triggered.connect(
            self.switch_log_level()
        )

        # 更新滚动日志
        self.log_handler.signal.connect(self.refresh_log)

        # 按下键盘表示停止任务的键
        self.listen_keyboard_thread.stop_key_press_signal.connect(
            self.stop_key_pressed
        )

        # 未实现功能
        self.ui.browse_pic.clicked.connect(self.msg_box)

    def init_model(self) -> None:

        # 游戏选择下拉框中列出的游戏
        self.game_model = QStandardItemModel(self)
        self.ui.game_choose.setModel(self.game_model)

        # 所有任务框中列出的任务
        self.task_model = QStandardItemModel(self)
        self.ui.all_tasks.setModel(self.task_model)

        # 所有窗口框中列出的窗口名
        self.window_model = QStandardItemModel(self)
        self.ui.all_windows.setModel(self.window_model)

    def init_plugin(self) -> None:
        """
        初始化插件,会寻找插件目录所有模块中的BaseGame子类,
        使用_gamename_属性进行对应,选择相应游戏时使用该子类
        的实例运行任务
        """

        sys.path.append("./plugins")
        app_workdir = os.getcwd()

        for module_str in os.listdir("./plugins"):
            if (
                    os.path.isfile(f"./plugins/{module_str}")
                    and not module_str.endswith(".py")
            ):
                continue

            if os.path.isdir(f"./plugins/{module_str}"):
                os.chdir(f"./plugins/{module_str}")

            try:
                module = importlib.import_module(module_str)
            except ImportError:
                continue

            for attr_str in dir(module):
                if attr_str.startswith("__"):
                    continue
                factory = getattr(module, attr_str)
                if isinstance(factory, type) and issubclass(factory, BaseGame):
                    factory = cast(BaseGame, factory)
                    self.game_factory[factory._gamename_] = factory

        os.chdir(app_workdir)

    def init_game_choose_box(self) -> None:
        """初始化游戏选择下拉框,并设置默认游戏
        """

        for game in set(chain(self.game_pic.keys(), self.game_factory.keys())):
            item = QStandardItem(game)
            item.setSizeHint(QSize(400, 50))
            item.setData(game)
            self.game_model.appendRow(item)

    def init_game_and_window(self) -> None:
        """设置默认游戏及窗口
        """

        # 设置默认游戏
        default_game = "牧羊人之心"
        self.ui.game_choose.setCurrentText(default_game)
        self.display_game_pic(default_game)

        # 设置默认窗口
        for name in _all_windows():
            if "模拟器" in name:
                break
        else:
            return None
        self.window_name = name
        self.ui.window_name.setText(name)
        self.auto_game.load_window(name)

    def display_game_pic(self, game: str):
        """设置`self.ui.game_pic`标签展示的图片,初始化BaseGame

        Args:
            game: `self.ui.game_choose`选中的游戏名称
        """

        # TODO 目前只能前台点击鼠标, 想换游戏要先暂停任务, 先省去这一步判断, 后续加上

        pic = QPixmap(self.game_pic[game])
        self.ui.game_pic.setPixmap(pic)
        self.ui.game_pic.setFixedSize(400, 400)
        self.ui.game_pic.setScaledContents(True)

        self.current_game = game
        self.auto_game = self.game_factory[game](
            pic_path=f"./common/{self.current_game}/"
        )

        self.task_load()

    def task_load(self) -> None:
        """列出当前游戏的所有任务
        """

        # 列出之前先清空
        self.task_model.removeRows(0, self.task_model.rowCount())

        with open(f"./common/{self.current_game}/autogame.task",
                  "r",
                  encoding="utf-8") as f:
            for i, line in enumerate(f.readlines()):
                task_item = TaskItem(AutoGameTask(line))
                task_item.run.clicked.connect(self.task_run(task_item))
                task_item.edit.clicked.connect(self.task_edit(task_item.task))
                task_item.delete.clicked.connect(
                    self.task_delete(task_item.task)
                )
                item = QStandardItem()
                item.setSizeHint(QSize(500, 50))
                item.setSelectable(False)
                self.task_model.appendRow(item)
                self.ui.all_tasks.setIndexWidget(item.index(), task_item)

    def choose_window(self) -> None:
        """列出所有前台窗口名称供选择
        """

        self.ui.func_area.setCurrentWidget(self.ui.window_choose_widget)
        self.window_model.removeRows(0, self.window_model.rowCount())

        for name in _all_windows():
            item = QStandardItem(name)
            item.setSizeHint(QSize(500, 50))
            self.window_model.appendRow(item)

    def set_window(self) -> None:
        """设置需要点击的窗口名
        """

        self.window_name = self.ui.all_windows.currentIndex().data()
        self.ui.window_name.setText(self.window_name)
        self.auto_game.load_window(self.window_name)
        self.return_home()

    def print_screen(self) -> None:
        """调用截图dll进行截图,并将功能区切至保存截图处
        """

        if self.current_game is None:
            QMessageBox.about(self.main_window, "注意", "请先选择游戏")
            return

        self.capture_dll.PrScrn()
        self.ui.func_area.setCurrentWidget(self.ui.capture_widget)
        clipboard = QApplication.clipboard()
        self.ui.capture_result.setPixmap(clipboard.pixmap())
        self.ui.capture_result.setFixedSize(400, 400)

    def save_img(self) -> None:
        """保存截取的图片
        """

        name = self.ui.captured_img_name.text()  # TODO 未选择游戏时先提示选择and已有同名文件时提示
        if not name:
            return
        image = ImageGrab.grabclipboard()
        image.save(f"./common/{self.current_game}/{name}.jpg")
        self.auto_game.load_pic(pic_path=f"./common/{self.current_game}/")
        self.return_home()

    def switch_click_method(self, method: str) -> Callable:
        """切换点击方式,点击相应菜单后调用

        Args:
            method: 目标点击方法,"pywin32"或"pynput"
        """

        def wrapper():

            if self.auto_game is None:

                self.ui.pywin32_chosen.setChecked(True)
                self.ui.pynput_chosen.setChecked(False)
                self.logger.info("未选择游戏,当前点击方式: [pywin32]")
                return None
            self.auto_game.switch_click_method(method)
            if method == "pywin32":
                self.ui.pywin32_chosen.setChecked(True)
                self.ui.pynput_chosen.setChecked(False)
            elif method == "pynput":
                self.ui.pywin32_chosen.setChecked(False)
                self.ui.pynput_chosen.setChecked(True)
            self.logger.info(f"当前点击方式: [{method}]")

        return wrapper

    def switch_log_level(self) -> Callable:
        """切换日志等级
        """

        def wrapper():

            if self.ui.log_level_switch.isChecked():
                self.logger.setLevel(logging.DEBUG)
                self.log_handler.queue.change_max_size(100)
                self.logger.info("日志debug模式已打开")
            else:
                self.logger.setLevel(logging.INFO)
                self.log_handler.queue.change_max_size(50)
                self.logger.info("日志debug模式已关闭")

        return wrapper

    def return_home(self) -> None:
        """回到主界面,即任务选择界面
        """

        self.ui.func_area.setCurrentWidget(self.ui.task_widget)

    def stop_key_pressed(self) -> None:
        self.logger.info("按下F11键")
        if self.running is True:
            self.task_stop()
            self.ui.game_choose.setEnabled(True)
            self.running = False
            self.current_item.run.setIcon(self.ui.run_icon)
            self.current_task = cast(AutoGameTask, None)
            self.current_item = cast(TaskItem, None)
        else:
            self.logger.info("没有正在运行的任务")

    def task_run(self, item: TaskItem) -> Callable:
        """运行或停止自动点击任务

        Args:
            task: 因为要更改按钮图标, 故传入`TaskItem`
        """

        def wrapper():

            if self.running:  # 已有任务在运行
                if id(self.current_item) == id(item):
                    self.task_stop()
                    item.run.setIcon(self.ui.run_icon)
                else:
                    # 提示其他任务正在运行
                    pass

            else:  # 无任务正在运行
                if not self.window_name:  # 未选择要控制的窗口,先选择之
                    self.choose_window()
                else:
                    item.run.setIcon(self.ui.stop_icon)
                    self.ui.game_choose.setEnabled(False)
                    self.task_thread.load_task(self.auto_game, item.task)
                    self.task_thread.start()
                    self.running = True
                    self.current_task = item.task
                    self.current_item = item

        return wrapper

    def task_stop(self) -> None:
        """停止当前正在运行的任务
        """

        if self.running is False:
            self.logger.info("没有正在运行的任务")

        self.logger.info("正在中止任务...")
        self.auto_game.cancle()
        self.ui.game_choose.setEnabled(True)
        self.running = False
        self.current_task = cast(AutoGameTask, None)
        self.current_item = cast(TaskItem, None)

    def task_edit(self, task: AutoGameTask) -> Callable:
        """编辑任务

        Args:
            task: 自动点击任务
        """
        def wrapper():
            self.ui.func_area.setCurrentWidget(self.ui.edit_task_widget)
            self.ui.edit_task.load_game(self.auto_game)
            self.ui.edit_task.load(task)
            self.ui.edited_task_name.setText(task.name)
        return wrapper

    def task_editor_clear(self) -> None:
        """清空任务编辑区的数据
        """

        self.ui.edit_task.clear()
        self.ui.edited_task_name.setText("")

    def task_delete(self, task: AutoGameTask) -> Callable:
        """删除任务

        Args:
            task: 自动点击任务
        """
        def wrapper():  # TODO 询问是否删除

            old_file = f"./common/{self.current_game}/autogame.task"
            new_file = f"./common/{self.current_game}/autogame.tasknew"
            backup_file = f"./common/{self.current_game}/autogame.taskbackup"

            old = open(old_file, "r", encoding="utf-8")
            new = open(new_file, "wb")
            pattern = re.compile(r"\"name\":\"([^,\"\s]+)\"")
            for line in old.readlines():
                search = pattern.search(line)
                if (search is not None) and (search.group(1) == task.name):
                    line = ""
                new.write(line.encode("utf-8"))

            old.close()
            new.close()

            if os.path.isfile(backup_file):
                os.remove(backup_file)
            os.rename(old_file, backup_file)
            os.rename(new_file, old_file)

            # 停止当前任务并重新加载任务
            if self.running:
                self.task_stop()
            self.task_load()

        return wrapper

    def task_new(self) -> None:
        """新建任务
        """

        if self.current_game is None:
            QMessageBox.about(self.main_window, "注意", "请先选择游戏")
            return

        self.ui.game_choose.setEnabled(True)
        self.ui.func_area.setCurrentWidget(self.ui.edit_task_widget)
        self.ui.edit_task.load_game(self.auto_game)

    def task_save(self) -> None:  # TODO 载入任务时填入名称,新建同名任务
        """保存任务编辑界面的任务
        """

        task_dic = self.ui.edit_task.dump()  # type: Optional[TaskDic]
        if task_dic is None:
            return None

        name = self.ui.edited_task_name.text()
        if not name:
            return None  # TODO 文本框标红
        task_dic["name"] = name
        task_str = json.dumps(
            task_dic,
            ensure_ascii=False,
            separators=(",", ":")
        ) + "\n"

        old_file = f"./common/{self.current_game}/autogame.task"
        new_file = f"./common/{self.current_game}/autogame.tasknew"
        backup_file = f"./common/{self.current_game}/autogame.taskbackup"

        old = open(old_file, "r", encoding="utf-8")
        new = open(new_file, "wb")
        pattern = re.compile(r"\"name\":\"([^,\"\s]+)\"")
        write_flag = False
        for line in old.readlines():
            search = pattern.search(line)  # TODO 同名任务询问是否覆盖
            if (search is not None) and (search.group(1) == name):
                if write_flag is False:
                    line = task_str
                    write_flag = True
                else:
                    line = ""
            new.write(line.encode("utf-8"))
        if write_flag is not True:
            new.write(task_str.encode("utf-8"))

        old.close()
        new.close()

        if os.path.isfile(backup_file):
            os.remove(backup_file)
        os.rename(old_file, backup_file)
        os.rename(new_file, old_file)

        # 停止当前任务并重新加载任务
        if self.running:
            self.task_stop()
        self.task_load()

        # 返回运行任务界面
        self.return_home()

    def refresh_log(self, text: str) -> None:

        cursor = self.ui.log.textCursor()  # type: QTextCursor
        self.ui.log.setText(text)
        self.ui.log.moveCursor(cursor.End)

    def msg_box(self) -> None:

        QMessageBox.about(self.main_window, "豪华好礼", "首充6元解锁全部功能")

    def exec_(self) -> None:
        """窗口关闭时终止线程,防止窗口退出后线程仍在运行
        """

        self.app.exec_()
        if self.running:
            self.task_stop()
        self.task_thread.terminate()
        self.listen_keyboard_thread.terminate()

    def run(self):

        self.main_window.show()
        sys.exit(self.exec_())


if __name__ == "__main__":

    app = App()
    app.run()
