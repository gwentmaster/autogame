#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-07-01 16:14:02
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


"""gui界面设计"""


from typing import Tuple

from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QCursor, QFont, QIcon
from PyQt5.QtWidgets import (
    QAction,
    QComboBox,
    QFrame,
    QGridLayout,
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QListView,
    QMainWindow,
    QPushButton,
    QStackedWidget,
    QTextBrowser,
    QVBoxLayout,
    QWidget,
)

from taskeditor import EditTaskView


class UiMainWindow(object):

    def setupUi(self, main_window: QMainWindow):

        self.return_icon = QIcon("./common/application/return.jpg")
        self.confirm_icon = QIcon("./common/application/confirm.jpg")
        self.clear_icon = QIcon("./common/application/clear.jpg")
        self.run_icon = QIcon("./common/application/run.jpg")
        self.stop_icon = QIcon("./common/application/stop.jpg")
        self.edit_icon = QIcon("./common/application/edit.jpg")
        self.delete_icon = QIcon("./common/application/delete.jpg")

        main_window.setWindowTitle("AutoGame")

        self.widget = QWidget(main_window)
        main_window.resize(1111, 823)
        main_window.setCentralWidget(self.widget)

        """=======================菜单栏========================"""

        self.menu_bar = main_window.menuBar()

        # 切换点击方式
        self.click_method_choose = self.menu_bar.addMenu("点击方式")
        self.pywin32_chosen = (
            self.click_method_choose.addAction("pywin32")
        )  # type: QAction
        self.pynput_chosen = (
            self.click_method_choose.addAction("pynput")
        )  # type: QAction
        self.pywin32_chosen.setCheckable(True)
        self.pywin32_chosen.setChecked(True)
        self.pynput_chosen.setCheckable(True)
        self.pynput_chosen.setChecked(False)

        # 设置日志等级
        self.log_level_menu = self.menu_bar.addMenu("日志")
        self.log_level_switch = (
            self.log_level_menu.addAction("debug模式")
        )  # type: QAction
        self.log_level_switch.setCheckable(True)
        self.log_level_switch.setChecked(False)

        """=====================游戏选择下拉框======================"""

        self.game_choose = QComboBox(self.widget, minimumWidth=200)
        self.game_choose.setPlaceholderText("请选择游戏 ")
        self.game_choose.setFixedSize(QSize(400, 50))
        self.game_pic = QLabel(self.widget)
        self.game_pic.setFixedSize(QSize(400, 400))
        self.game_choose_layout = QVBoxLayout()
        self.game_choose_layout.addWidget(self.game_choose)
        self.game_choose_layout.addWidget(self.game_pic)

        """======================功能按钮区======================="""

        self.create_task = QPushButton("新建任务", self.widget)
        self.create_task.setFixedHeight(50)
        self.create_task.setCursor(QCursor(Qt.PointingHandCursor))
        self.browse_pic = QPushButton("浏览图片", self.widget)
        self.browse_pic.setFixedHeight(50)
        self.browse_pic.setCursor(QCursor(Qt.PointingHandCursor))
        self.capture_screen = QPushButton("截图", self.widget)
        self.capture_screen.setFixedHeight(50)
        self.capture_screen.setCursor(QCursor(Qt.PointingHandCursor))
        self.window_choose = QPushButton("选择窗口", self.widget)
        self.window_choose.setFixedHeight(50)
        self.window_choose.setCursor(QCursor(Qt.PointingHandCursor))
        self.often_command_layout = QGridLayout()
        self.often_command_layout.addWidget(self.create_task, 0, 0)
        self.often_command_layout.addWidget(self.browse_pic, 0, 1)
        self.often_command_layout.addWidget(self.capture_screen, 1, 0)
        self.often_command_layout.addWidget(self.window_choose, 1, 1)

        self.left_top_layout = QVBoxLayout()
        self.left_top_layout.addLayout(self.game_choose_layout)
        self.left_top_layout.addLayout(self.often_command_layout)

        """=====================任务运行功能区======================"""

        self.task_widget = QWidget(self.widget)
        self.window_name_label = QLabel("当前控制窗口:", self.widget)
        self.window_name = QLabel(self.widget)
        self.running_task_label = QLabel("正在运行的任务:", self.widget)
        self.running_task_label.setFixedSize(100, 100)
        self.running_task = QLabel(self.widget)
        self.all_tasks_label = QLabel("所有任务:", self.widget)
        self.all_tasks_label.setFixedSize(100, 100)
        self.all_tasks = QListView(self.widget)

        self.task_layout = QVBoxLayout()
        self.task_layout.addWidget(self.window_name_label)
        self.task_layout.addWidget(self.window_name)
        self.task_layout.addWidget(self.running_task_label)
        self.task_layout.addWidget(self.running_task)
        self.task_layout.addWidget(self.all_tasks_label)
        self.task_layout.addWidget(self.all_tasks)
        self.task_widget.setLayout(self.task_layout)

        """====================编辑/新建任务功能区===================="""

        self.edit_task_widget = QWidget(self.widget)
        self.edit_task = EditTaskView(parent=self.widget)
        self.edit_task.setFixedSize(800, 500)
        self.edited_task_name = QLineEdit(self.widget)
        self.edited_task_name.setPlaceholderText("请输入任务名称")
        self.edited_task_name.setFont(QFont("Microsoft YaHei", 20))
        self.edit_task_return = QPushButton(self.widget)
        self.setup_button(
            button=self.edit_task_return,
            size=(100, 50),
            icon=self.return_icon,
            icon_size=QSize(100, 50),
            flat=True
        )
        self.edit_task_clear = QPushButton(self.widget)
        self.setup_button(
            button=self.edit_task_clear,
            size=(100, 50),
            icon=self.clear_icon,
            icon_size=QSize(100, 50),
            flat=True
        )
        self.edit_task_confirm = QPushButton(self.widget)
        self.setup_button(
            button=self.edit_task_confirm,
            size=(100, 50),
            icon=self.confirm_icon,
            icon_size=QSize(100, 50),
            flat=True
        )

        self.edit_task_layout = QGridLayout(self.edit_task_widget)
        self.edit_task_layout.addWidget(
            self.edit_task,
            0, 0, 1, 3,
            alignment=Qt.Alignment(Qt.AlignCenter)
        )
        self.edit_task_layout.addWidget(self.edited_task_name, 1, 0, 1, 3)
        self.edit_task_layout.addWidget(self.edit_task_return, 2, 0)
        self.edit_task_layout.addWidget(self.edit_task_clear, 2, 1)
        self.edit_task_layout.addWidget(self.edit_task_confirm, 2, 2)

        """======================截图功能区======================="""

        self.capture_widget = QWidget(self.widget)
        self.capture_result = QLabel(self.widget)
        self.capture_result.setFrameShape(QFrame.Box)  # 边框
        self.captured_img_name = QLineEdit(self.widget)
        self.captured_img_name.setPlaceholderText("请输入图像名称")
        self.captured_img_name.setFont(QFont("Microsoft YaHei", 20))
        self.captured_img_name.setFixedHeight(30)
        self.capture_again = QPushButton("重新截取", self.widget)
        self.setup_button(button=self.capture_again, size=(300, 50))
        self.save_img = QPushButton("保存图片", self.widget)
        self.setup_button(button=self.save_img, size=(300, 50))
        self.capture_layout = QGridLayout()
        self.capture_layout.addWidget(
            self.capture_result,
            0, 0, 1, 2,
            alignment=Qt.Alignment(Qt.AlignCenter)
        )
        self.capture_layout.addWidget(self.captured_img_name, 1, 0, 1, 2)
        self.capture_layout.addWidget(self.capture_again, 2, 0)
        self.capture_layout.addWidget(self.save_img, 2, 1)
        self.capture_widget.setLayout(self.capture_layout)

        """=====================图片浏览功能区======================"""

        """=====================窗口选择功能区======================"""

        self.window_choose_widget = QWidget(self.widget)
        self.all_windows = QListView(self.widget)
        self.all_window_return = QPushButton(self.widget)
        self.setup_button(
            button=self.all_window_return,
            size=(300, 50),
            icon=self.return_icon,
            icon_size=QSize(200, 30),
            flat=True
        )
        self.all_window_confirm = QPushButton(self.widget)
        self.setup_button(
            button=self.all_window_confirm,
            size=(300, 50),
            icon=self.confirm_icon,
            icon_size=QSize(200, 30),
            flat=True
        )

        self.window_choose_layout = QGridLayout()
        self.window_choose_layout.addWidget(self.all_windows, 0, 0, 1, 2)
        self.window_choose_layout.addWidget(self.all_window_return, 1, 0)
        self.window_choose_layout.addWidget(self.all_window_confirm, 1, 1)
        self.window_choose_widget.setLayout(self.window_choose_layout)

        """=======================功能区========================"""

        self.func_area = QStackedWidget()
        self.func_area.addWidget(self.task_widget)
        self.func_area.addWidget(self.window_choose_widget)
        self.func_area.addWidget(self.capture_widget)
        self.func_area.addWidget(self.edit_task_widget)

        self.top_layout = QHBoxLayout()
        self.top_layout.addLayout(self.left_top_layout)
        self.top_layout.addWidget(self.func_area)

        """=======================日志区========================"""

        self.log = QTextBrowser()
        self.log.setFont(QFont("Microsoft YaHei", 20))

        """======================主窗口布局======================="""

        self.main_layout = QVBoxLayout(self.widget)
        self.main_layout.addLayout(self.top_layout)
        self.main_layout.addWidget(self.log)
        self.widget.setLayout(self.main_layout)

    def setup_button(
        self,
        button: QPushButton,
        size: Tuple[int, int] = None,
        icon: QIcon = None,
        icon_size: QSize = None,
        flat: bool = True
    ) -> None:

        if size:
            button.setFixedSize(*size)

        if icon:
            button.setIcon(icon)
            if icon_size:
                button.setIconSize(icon_size)

        button.setFlat(flat)
        button.setCursor(QCursor(Qt.PointingHandCursor))
