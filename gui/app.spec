# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['app.py'],
             pathex=['D:\\programs\\z\\Sublime Text\\Code\\MyScripts\\autogame\\gui', 'D:/programs/z/Sublime Text/Code/MyScripts/autogame'],
             binaries=[],
             datas=[],
             hiddenimports=["cv2", "win32api", "win32con", "win32gui", "win32ui", "PIL", "PIL.Image", "pynput", "pkg_resources.py2_warn", "autogame"],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='app',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False )
