#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-08-11 16:40:49
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


"""编辑任务相关组件"""


from itertools import chain
from typing import Any, Callable, cast, Dict, List, Optional, Union

from PyQt5.QtCore import (
    pyqtSignal,
    QSize,
    Qt
)
from PyQt5.QtGui import (
    QCursor,
    QIcon,
    QPixmap,
    QStandardItem,
    QStandardItemModel
)
from PyQt5.QtWidgets import (
    QApplication,
    QComboBox,
    QHBoxLayout,
    QLabel,
    QListView,
    QPushButton,
    QSpinBox,
    QStackedWidget,
    QWidget
)

# TODO
import sys
sys.path.insert(0, "../")
# TODO
from autogame.base import BaseGame
from autogame.task import AutoGameTask, TaskDic


class BaseArgsComponent(QWidget):
    """基本参数组件,在编辑任务界面使用
    """

    def __init__(self):
        super(BaseArgsComponent, self).__init__()

    def set_args(self, args: List) -> None:
        raise NotImplementedError()

    @property
    def args(self) -> Optional[List]:
        raise NotImplementedError()


class TimerComponent(BaseArgsComponent):
    """设置休眠时长的组件,用作sleep方法的参数

    Attributes:
        timer: 整数设置部件
        args: 获取组件代表的参数列表
    """

    def __init__(self):
        super(TimerComponent, self).__init__()

        # 0到3600s,步长为1分钟
        self.timer = QSpinBox(self)
        self.timer.setRange(0, 3600)
        self.timer.setSingleStep(60)

        self.second_label = QLabel("秒")

        # 布局
        self.layout = QHBoxLayout(self)
        self.layout.addWidget(self.timer)
        self.layout.addWidget(self.second_label)

    def set_args(self, args: List[int]) -> None:
        """设置timer的值

        Args:
            args: 只含一个整数的列表
        """

        self.timer.setValue(args[0])

    @property
    def args(self) -> List[int]:
        return [self.timer.value()]


class FigureComponent(BaseArgsComponent):
    """选择要点击的图片的组件,用作click方法的参数

    Args:
        game: 游戏类,从中获取图片名称及路径

    Attributes:
        before_choose: 需点击图片下拉框
        after_choose: 确认点击方式下拉框
        args: 获取组件代表的参数列表
    """

    def __init__(self, game: BaseGame):
        super(FigureComponent, self).__init__()
        self.game = game
        self._args = {
            "before": None,
            "after": None
        }  # type: Dict[str, Any]

        self.setup_ui()

    def setup_ui(self) -> None:
        """设置组件样式,两组下拉框加图片的组合
        """

        # 选择需要点击的图片
        self.before_choose = QComboBox(self)
        self.before_choose.combo_type = "figure_combo"  # QSS中使用
        self.before_choose.wheelEvent = lambda e: None
        self.before_pic_index = {}  # type: Dict[str, int]
        self.before_choose.setPlaceholderText("请选择要点击的图片")
        self.before_model = QStandardItemModel(self)
        self.before_choose.setModel(self.before_model)
        for i, pic_name in enumerate(self.game.picture):  # TODO 可设置排序,排序可放在BaseGame类中
            self.before_pic_index[pic_name] = i
            item = QStandardItem(pic_name)
            item.setData(pic_name)
            item.setSizeHint(QSize(0, 30))
            self.before_model.appendRow(item)

        # 展示图片的标签
        self.before_pic = QLabel()
        self.before_pic.label_type = "figure_label"  # QSS中使用

        # 选择用于验证的图片
        self.after_choose = QComboBox(self)
        self.after_choose.combo_type = "figure_combo"  # QSS中使用
        self.after_choose .wheelEvent = lambda e: None
        self.after_pic_index = {}  # type: Dict[str, int]
        self.after_choose.setPlaceholderText("请选择验证方式")
        self.after_model = QStandardItemModel(self)
        self.after_choose.setModel(self.after_model)
        for i, pic_name in enumerate(
            chain(("无", "找不到该图片"), self.game.picture)
        ):
            self.after_pic_index[pic_name] = i
            item = QStandardItem(pic_name)
            item.setData(pic_name)
            item.setSizeHint(QSize(0, 30))
            self.after_model.appendRow(item)

        # 展示验证图片的标签
        self.after_pic = QLabel()
        self.after_pic.label_type = "figure_label"  # QSS中使用

        # 下拉框选择好后,展示相应图片
        self.before_choose.textActivated.connect(self.set_before_pic)
        self.after_choose.textActivated.connect(self.set_after_pic)

        # 水平布局
        self.layout = QHBoxLayout(self)
        self.layout.addWidget(
            self.before_choose,
            alignment=Qt.Alignment(Qt.AlignVCenter)
        )
        self.layout.addWidget(
            self.before_pic,
            alignment=Qt.Alignment(Qt.AlignVCenter)
        )
        self.layout.addWidget(
            self.after_choose,
            alignment=Qt.Alignment(Qt.AlignVCenter)
        )
        self.layout.addWidget(
            self.after_pic,
            alignment=Qt.Alignment(Qt.AlignVCenter)
        )

    def set_before_pic(self, pic_name: str) -> None:
        """第一个下拉框选出时展示选中的图片
        """

        picture = QPixmap(str(self.game.picture[pic_name]))
        self.before_pic.setPixmap(picture)
        self._args["before"] = pic_name

    def set_after_pic(self, pic_name: str) -> None:
        """
        第二个下拉框选出时展示选中的图片
        """

        # 此两项不是图片,选中时清空图片展示
        if pic_name in ("无", "找不到该图片"):
            self.after_pic.setPixmap(QPixmap())
            if pic_name == "找不到该图片":
                self._args["after"] = "not_find"
            return None

        picture = QPixmap(str(self.game.picture[pic_name]))
        self.after_pic.setPixmap(picture)

    def set_args(self, args: List[Union[str, float]]) -> None:
        """加载相应参数

        Args:
            args: 参数列表
        """

        before_pic = cast(str, args[0])
        self.before_choose.setCurrentIndex(self.before_pic_index[before_pic])
        self.set_before_pic(before_pic)

        if len(args) == 1:  # TODO
            after_arg = None
        else:
            after_arg = cast(Optional[str], args[1])
        try:
            after_pic = {
                None: "无",
                "not_find": "找不到该图片"
            }[after_arg]
        except KeyError:
            after_pic = cast(str, after_arg)
        self.after_choose.setCurrentIndex(self.after_pic_index[after_pic])
        self.set_after_pic(after_pic)

    @property
    def args(self) -> Optional[List]:
        if self._args["before"] is None:
            return None
        return [self._args[key] for key in ("before", "after")]


class SingleFigureComponent(BaseArgsComponent):
    """单个图片的组件,用作find方法的参数

    Args:
        game: 游戏类,从中获取图片名称及路径

    Attributes:
        args: 获取组件代表的参数列表
    """

    def __init__(self, game: BaseGame):

        super(SingleFigureComponent, self).__init__()
        self.game = game
        self._args = {
            "pic": None,
            "threshold": None
        }  # type: Dict[str, Any]

        self.setup_ui()

    def setup_ui(self) -> None:

        # 图片选择下拉框
        self.pic_choose = QComboBox(self)
        self.pic_choose.combo_type = "figure_combo"  # QSS中使用
        self.pic_choose.setPlaceholderText("请选择图片")
        self.pic_choose.wheelEvent = lambda e: None
        self.pic_index = {}  # type: Dict[str, int]
        self.pic_model = QStandardItemModel(self)
        self.pic_choose.setModel(self.pic_model)
        for i, pic_name in enumerate(self.game.picture):
            self.pic_index[pic_name] = i
            item = QStandardItem(pic_name)
            item.setData(pic_name)
            item.setSizeHint(QSize(0, 30))
            self.pic_model.appendRow(item)

        # 展示图片的标签
        self.pic_label = QLabel(self)
        self.pic_label.label_type = "figure_label"

        # 阈值设置
        self.threshold_label = QLabel("查找阈值(%)")
        self.threshold_setter = QSpinBox(self)
        self.threshold_setter.setRange(10, 100)
        self.threshold_setter.setSingleStep(10)
        self.threshold_setter.setValue(50)  # 默认为0.5

        # 下拉框选择好后,展示相应图片
        self.pic_choose.textActivated.connect(self.set_pic)

        # 横向布局
        self.layout = QHBoxLayout(self)
        self.layout.addWidget(
            self.pic_choose,
            alignment=Qt.Alignment(Qt.AlignVCenter)
        )
        self.layout.addWidget(
            self.pic_label,
            alignment=Qt.Alignment(Qt.AlignVCenter)
        )
        self.layout.addWidget(
            self.threshold_label,
            alignment=Qt.Alignment(Qt.AlignVCenter)
        )
        self.layout.addWidget(
            self.threshold_setter,
            alignment=Qt.Alignment(Qt.AlignVCenter)
        )

    def set_pic(self, pic_name: str) -> None:
        """设置图片展示标签中展示的图片并更新参数列表

        Args:
            pic_name: 被选择的图片的名称
        """

        picture = QPixmap(str(self.game.picture[pic_name]))
        self.pic_label.setPixmap(picture)
        self.pic_label.setScaledContents(True)
        self._args["pic"] = pic_name

    def set_args(self, args: List) -> None:
        """加载参数列表

        Args:
            args: 参数列表
        """

        pic_name = cast(str, args[0])
        try:
            threshold = int(args[1] * 100)
        except IndexError:
            threshold = 50

        self.pic_choose.setCurrentIndex(self.pic_index[pic_name])
        self.set_pic(pic_name)
        self.threshold_setter.setValue(threshold)

    @property
    def args(self) -> Optional[List]:
        """获取参数列表,未选择图片则为None
        """

        if self._args["pic"] is None:
            return None
        self._args["threshold"] = self.threshold_setter.value() / 100
        return [self._args[key] for key in ("pic", "threshold")]


class MethodComponent(QWidget):
    """
    方法组件

    Args:
        method_dic: 方法名为键,展示名称为值的字典,
                    如{"click": "点击"}

    Attributes:
        set_method_signal: 从下拉框中选择了方法后发出的信号,
                           用于更换参数组件
        method: 获取部件代表的方法名称
    """

    set_method_signal = pyqtSignal(str)

    def __init__(self, method_dic: Dict[str, str]):

        super(MethodComponent, self).__init__()
        self._method = None  # type: Optional[str]
        self.method_dic = method_dic
        self.method_choose = QComboBox(self)
        self.method_choose.combo_type = "method_combo"
        self.method_choose.setFixedSize(100, 30)
        self.method_choose.wheelEvent = lambda e: None
        self.method_choose.setPlaceholderText("请选择方法")
        self.model = QStandardItemModel(self)
        self.method_choose.setModel(self.model)

        self.method_index = {}  # type: Dict[str, int]

        for i, pair in enumerate(self.method_dic.items()):
            data, display_name = pair
            self.method_index[data] = i
            item = QStandardItem(display_name)
            item.setData(data)
            item.setSizeHint(QSize(0, 30))
            self.model.appendRow(item)

        self.layout = QHBoxLayout(self)
        self.layout.addWidget(
            self.method_choose,
            alignment=Qt.Alignment(Qt.AlignVCenter)
        )

        self.method_choose.textActivated.connect(self.method_chosen)

    @property
    def method(self) -> Optional[str]:
        return self._method

    def method_chosen(self, m: str) -> None:
        """选择好方法时调用,发送方法选择信号

        Args:
            m: 选择好的方法的展示名称
        """

        for k, v in self.method_dic.items():
            if v == m:
                method = k
                break
        self.set_method_signal.emit(method)
        self._method = method

    def set_method(self, method: str) -> None:
        """加载方法,发送方法选择信号

        Args:
            method: 方法名称
        """

        self.method_choose.setCurrentIndex(self.method_index[method])
        self.method_chosen(self.method_dic[method])


class BaseTaskComponent(QWidget):

    def __init__(self):
        super(BaseTaskComponent, self).__init__()

    @property
    def method_component(self) -> MethodComponent:
        raise NotImplementedError()

    @property
    def args_component(self) -> BaseArgsComponent:
        raise NotImplementedError()

    @property
    def method(self) -> Optional[str]:
        raise NotImplementedError()

    @property
    def args(self) -> Optional[List]:
        raise NotImplementedError()


class IfComponent(BaseTaskComponent):

    def __init__(self, game: BaseGame):
        super(IfComponent, self).__init__()
        self.game = game

        self.if_label = QLabel("如果")

        self._method_component = MethodComponent({"find": "找到"})
        self._args_component = BaseArgsComponent()
        self._method_component.setFixedSize(400, 200)

        self.args_stack = QStackedWidget()
        self.args_stack.addWidget(self._args_component)
        self.args_stack.setCurrentWidget(self._args_component)

        self._method_component.set_method_signal.connect(
            self.set_args_component
        )

        self.layout = QHBoxLayout(self)
        self.layout.addWidget(self.if_label)
        self.layout.addWidget(self._method_component)
        self.layout.addWidget(self.args_stack)

    @property
    def method_component(self) -> MethodComponent:
        return self._method_component

    @property
    def args_component(self) -> BaseArgsComponent:
        return self._args_component

    @property
    def method(self) -> Optional[str]:
        return self._method_component.method

    @property
    def args(self) -> Optional[List]:
        return self._args_component.args

    def set_args_component(self, method: str) -> None:
        """根据选中的方法,选择相应的参数组件

        Args:
            method: 被选中的方法
        """

        self.args_stack.removeWidget(self._args_component)

        if method == "find":
            self._args_component = SingleFigureComponent(self.game)

        self.args_stack.addWidget(self._args_component)
        self.args_stack.setCurrentWidget(self._args_component)

    def set_method(self, method: str) -> None:
        self._method_component.set_method(method)

    def set_args(self, args: List) -> None:
        self._args_component.set_args(args)

    def clear(self) -> None:
        """清空选择
        """

        self._method_component.method_choose.setCurrentText("请选择方法")
        self.args_stack.removeWidget(self._args_component)
        self._args_component = BaseArgsComponent()
        self.args_stack.addWidget(self._args_component)
        self.args_stack.setCurrentWidget(self._args_component)


class TaskComponent(BaseTaskComponent):
    """任务组件
    """

    def __init__(self, game: BaseGame):

        super(TaskComponent, self).__init__()

        self.game = game
        self._method_component = MethodComponent(
            {"click": "点击", "sleep": "等待"}
        )
        self._args_component = BaseArgsComponent()
        self._method_component.setFixedSize(400, 200)

        # 展示参数组件的区域
        self.args_stack = QStackedWidget()
        self.args_stack.addWidget(self._args_component)
        self.args_stack.setCurrentWidget(self._args_component)

        # 选择好方法后,展示相应参数组件
        self._method_component.set_method_signal.connect(
            self.set_args_component
        )

        # 横向布局
        self.layout = QHBoxLayout(self)
        self.layout.addWidget(self._method_component)
        self.layout.addWidget(self.args_stack)

    @property
    def method_component(self) -> MethodComponent:
        return self._method_component

    @property
    def args_component(self) -> BaseArgsComponent:
        return self._args_component

    @property
    def method(self) -> Optional[str]:
        return self._method_component.method

    @property
    def args(self) -> Optional[List]:
        return self._args_component.args

    def set_args_component(self, method: str) -> None:
        """根据选中的方法,选择相应的参数组件

        Args:
            method: 被选中的方法
        """

        self.args_stack.removeWidget(self._args_component)

        if method == "click":
            self._args_component = FigureComponent(self.game)
        elif method == "sleep":
            self._args_component = TimerComponent()

        self.args_stack.addWidget(self._args_component)
        self.args_stack.setCurrentWidget(self._args_component)

    def set_method(self, method: str) -> None:

        self._method_component.set_method(method)

    def set_args(self, args: List) -> None:

        self._args_component.set_args(args)


class EditTaskView(QListView):
    """编辑任务区域

    Args:
        game:
    """

    def __init__(self, game: BaseGame = None, *args, **kwargs):

        super(EditTaskView, self).__init__(*args, **kwargs)
        self.game = game
        self._widgets = []  # type: List[TaskComponent]
        self.init_model()

    def init_model(self) -> None:
        """初始化模型,设置好按钮点击的响应
        """

        self.model = QStandardItemModel()
        self.setModel(self.model)

        # 条件组件
        self.if_component = IfComponent(self.game)

        # 添加任务按钮
        self.if_add_button = QPushButton()
        self.if_add_button.setIcon(QIcon("./common/application/add.jpg"))
        self.if_add_button.setFlat(True)
        self.if_add_button.setCursor(QCursor(Qt.PointingHandCursor))

        # 否则标签
        self.else_label = QLabel("否则")

        # 添加else任务按钮
        self.else_add_button = QPushButton()
        self.else_add_button.setIcon(QIcon("./common/application/add.jpg"))
        self.else_add_button.setFlat(True)
        self.else_add_button.setCursor(QCursor(Qt.PointingHandCursor))

        # 添加并展示上面4个组件
        item = QStandardItem()
        item.setSelectable(False)
        item.setSizeHint(QSize(700, 200))
        self.model.appendRow(item)
        self._widgets.append(self.if_component)
        self.setIndexWidget(item.index(), self.if_component)
        for widget in ("if_add_button", "else_label", "else_add_button"):
            item = QStandardItem()
            item.setSelectable(False)
            item.setSizeHint(QSize(700, 100))
            self.model.appendRow(item)
            self._widgets.append(getattr(self, widget))
            self.setIndexWidget(item.index(), getattr(self, widget))

        # 记录两个按钮的索引
        self.if_add_button_index = 1
        self.else_add_button_index = 3

        # 点击添加按钮,添加任务组件
        self.if_add_button.clicked.connect(
            self.add_task_component("if_add_button_index")
        )
        self.else_add_button.clicked.connect(
            self.add_task_component("else_add_button_index")
        )

    def load_game(self, game: BaseGame) -> None:
        """
        加载游戏,对于需要先初始化的情况,可以先令__init__函数中的
        game参数为None,真正使用前再调用此函数即可

        Args:
            game: 游戏类
        """

        self.game = game
        for component in self._widgets:
            component.game = game

    def add_task_component(self, index_type: str) -> Callable:
        """添加任务组件

        Args:
            index_type: "if_add_button_index","else_add_button_index"
                        二者之一,用于区分是哪个按钮被点击了
        """
        def wrapper():
            index = getattr(self, index_type)
            component = TaskComponent(self.game)
            item = QStandardItem()
            item.setSizeHint(QSize(700, 200))
            item.setSelectable(False)
            self.model.insertRow(index, item)
            self._widgets.insert(index, component)
            self.setIndexWidget(item.index(), component)
            setattr(self, index_type, index + 1)

            # 上面的按钮被点击,下面按钮的索引数也要加1
            if index_type == "if_add_button_index":
                setattr(
                    self,
                    "else_add_button_index",
                    getattr(self, "else_add_button_index") + 1
                )

        return wrapper

    def clear(self) -> None:
        """清除所有任务组件,还原为初始状态
        """

        # 第一个的条件判断组件,否则标签以及两个添加任务按钮不用清除
        need_not_remove = [
            0,
            self.if_add_button_index,
            self.if_add_button_index + 1,
            self.else_add_button_index
        ]

        # 还原组件列表
        self._widgets = [
            self._widgets[i] for i in need_not_remove
        ]

        # 清空if_component的选择
        if_component = self._widgets[0]  # type: IfComponent
        if_component.clear()

        # 清空if_component与if_add_button间的组件
        while self.if_add_button_index > 1:
            self.model.removeRow(1)
            self.if_add_button_index -= 1
            self.else_add_button_index -= 1

        # 清空else_label与else_add_button间的组件
        while self.else_add_button_index > 3:
            self.model.removeRow(3)
            self.else_add_button_index -= 1

    def load(self, task: Union[AutoGameTask, Dict]) -> None:
        """加载任务

        Args:
            task: 任务字典或任务类
        """

        # 首先还原为初始状态
        self.clear()

        # 输入字典则转为任务类
        if isinstance(task, dict):
            task = AutoGameTask(task)

        if_component = self._widgets[0]  # type: IfComponent
        if_component.set_method(task.if_condition.method)
        if_component.set_args(task.if_condition.args)

        for t in task.tasks:
            self.add_task_component("if_add_button_index")()
            component = (
                self._widgets[self.if_add_button_index - 1]
            )  # type: TaskComponent
            component.set_method(t.method)
            component.set_args(t.args)

        for t in task.else_tasks:
            self.add_task_component("else_add_button_index")()
            component = (
                self._widgets[self.else_add_button_index - 1]
            )
            component.set_method(t.method)
            component.set_args(t.args)

    def dump(self) -> Optional[TaskDic]:
        """生成任务字典,有组件未进行选择则返回None,并将该组件标红
        """

        result = {"tasks": [], "else": []}  # type: TaskDic
        key = "tasks"
        for i, widget in enumerate(self._widgets):

            # 跳过两个按钮
            if i in (self.if_add_button_index, self.else_add_button_index):
                continue

            # 从"否则"标签往下全是"else"的内容
            if i == (self.if_add_button_index + 1):
                key = "else"
                continue

            # 有未做出选择的,直接返回None,相应组件标红
            method = widget.method
            if not method:
                widget.method_component.setStyleSheet(
                    "MethodComponent {border: 2px, solid, red;}"
                )
                return None
            args = widget.args
            if not args:
                widget.args_component.setStyleSheet(
                    "BaseArgsComponent {border: 2px, solid, red;}"
                )
                return None

            # 第一项为"if"条件
            if i == 0:
                result["if"] = {"method": method, "args": args}
                continue

            # 添加组件代表的任务
            result[key].append({"method": method, "args": args})

        if (result["tasks"] == []) and (result["else"] == []):
            return None
        return result


if __name__ == "__main__":

    app = QApplication(sys.argv)
    game = BaseGame("Code", "./common/application/")

    # m = FigureComponent(game)
    # m = TimerComponent()

    # m = TaskComponent(game)
    # m.show()
    # main_window = QMainWindow()

    v = EditTaskView(game)
    v.show()

    from concurrent.futures import ThreadPoolExecutor
    import time
    executor = ThreadPoolExecutor(1)
    executor.submit(time.sleep, 5)
    def hh():
        d = v.dump()
        print(d)
    executor.submit(hh)

    sys.exit(app.exec_())
