#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-08-01 07:28:13
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


"""日志处理,用于在gui界面的日志区生成滚动日志"""


import datetime as dt
import logging
import tempfile
from collections import deque
from typing import Deque

from PIL import Image
from PyQt5.QtCore import pyqtSignal


class LogQueue(object):
    """记录日志的类

    Args:
        max_size: 最大日志条数
    """

    def __init__(self, max_size: int):

        self.length_queue: Deque[int] = deque()  # 记录每条日志的长度
        self.log_string = ""
        self.size = 0
        self.max_size = max_size
        self.full = False

    def append(self, item: str) -> str:
        """添加日志,超过最大日志数则删除最老的日志

        Args:
            item: 要添加的日志

        Returns:
            日志字符串
        """

        append_length = len(item) + 1  # 加上一个换行符后的长度

        if self.full:
            self.length_queue.append(append_length)
            self.log_string = (
                self.log_string[self.length_queue.popleft():]
                + item + "\n"
            )
            return f"<html>{self.log_string}</html>"

        self.length_queue.append(append_length)
        self.log_string += (item + "\n")
        self.size += 1
        if self.size >= self.max_size:
            self.full = True
        return f"<html>{self.log_string}</html>"

    def change_max_size(self, max_size: int) -> None:
        """更改最大日志条数
        """

        if max_size == self.max_size:
            return None

        if max_size > self.max_size:
            self.max_size = max_size
            self.full = False
            return None

        self.max_size = max_size

        if self.size <= max_size:
            return None

        pop_length = 0
        for _ in range(self.size - max_size):
            pop_length += self.length_queue.popleft()
        self.log_string = self.log_string[pop_length:]

        return None


class RollingHandler(logging.Handler):
    """在ui界面实现滚动日志的类

    Args:
        max_size: 日志最大条数
        signal: 日志记录时需要发送的信号,因日志记录需要在
                子线程中进行,而pyqt不允许在子线程中新建QObject实例,
                故作为参数传入而不是作为自身属性
    """
    def __init__(self, max_size: int, signal: pyqtSignal):
        super(RollingHandler, self).__init__()
        logging.Handler.__init__(self)
        self.queue = LogQueue(max_size)
        self.signal = signal
        self.temp_dir = tempfile.TemporaryDirectory(dir="./cache")
        self.temp_files = []  # type: ignore[var-annotated]
        self.temp_file_boundary = max_size
        self.full = False

    def emit(self, record: logging.LogRecord):
        """格式化日志,发送日记记录信号,输出要显示的日志内容及长度
        """

        t = (
            dt.datetime
            .fromtimestamp(int(record.created))
            .strftime("%H:%M:%S")
        )

        if hasattr(record, "image"):
            file = tempfile.mkstemp(
                dir=self.temp_dir.name,
                suffix=".png"
            )[1]
            if self.full:
                self.temp_files.pop(0).delete()
            else:
                self.full = (
                    True
                    if self.full >= (self.temp_file_boundary - 1)
                    else False
                )
            self.temp_files.append(file)
            img = record.__dict__["image"]  # type: Image.Image
            img.save(file)
            msg = f"<img src=\"{file}\" />"
        else:
            msg = f"<p>{t} - {record.msg}</p>"

        log_text = self.queue.append(msg)
        self.signal.emit(log_text)
