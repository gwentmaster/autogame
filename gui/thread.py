#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2020-08-01 07:37:24
# @Author  : gwentmaster(1950251906@qq.com)
# I regret in my life


"""处理不同任务的子线程"""


from pynput import keyboard
from PyQt5.QtCore import pyqtSignal, QObject, QThread

# TODO
import sys
sys.path.insert(0, "../")
# TODO
from autogame.base import BaseGame
from autogame.exceptions import StopTaskException
from autogame.task import AutoGameTask


class TaskThread(QThread):
    """用于运行任务的子线程

    Args:
        parent: 父组件

    Attributes:
        log_signal: 由`RollingHandler`类在日志记录时发出
    """
    log_signal = pyqtSignal(str, name="log_signal")

    def __init__(self, parent: QObject):
        super().__init__(parent)
        self.game = None
        self.task = None

    def load_task(self, game: BaseGame, task: AutoGameTask) -> None:
        """加载任务

        Args:
            game: 运行任务的类
            task: 自动点击任务
        """

        self.game = game
        self.task = task

    def run(self) -> None:
        """运行任务
        """

        if not isinstance(self.game, BaseGame):
            raise TypeError("运行任务前需先加载任务")

        try:
            self.game.run_task(self.task)
        except StopTaskException:
            pass


class ListenKeyboardThread(QThread):
    """用于监听键盘按键的子线程

    Args:
        parent: 父组件

    Attributes:
        stop_key_press_signal: f11键按下时发出,用于中止任务
    """

    stop_key_press_signal = pyqtSignal(name="esc_press_signal")

    def __init__(self, parent: QObject):
        super().__init__(parent)

    def on_press(self, key) -> None:
        if key == keyboard.Key.f11:
            self.stop_key_press_signal.emit()

    def run(self) -> None:
        with keyboard.Listener(on_press=self.on_press) as listener:
            listener.join()
