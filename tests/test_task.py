# -*- coding: utf-8 -*-


import pytest

from autogame.task import SingleTask, AutoGameTask


def test_add():

    task = AutoGameTask("test")
    assert task.tasks == []

    task.add("click", "arg_1", "arg_2")
    assert task.tasks == [SingleTask(method="click", args=("arg_1", "arg_2"))]


def test_load_dump():

    task = AutoGameTask("test")

    # config格式错误
    config = {"key_1": "value_1", "key_2": "value_2"}
    with pytest.raises(AssertionError):
        task.load(config)

    # load
    config = {
        "test_task": [{"method": "method_1", "args": ["arg_1", "arg_2"]},
                      {"method": "method_2", "args": ["arg_3", "arg_4"]}]
    }
    task.load(config)
    assert len(task.tasks) == 2
    assert task.tasks[0] == SingleTask(method="method_1",
                                       args=["arg_1", "arg_2"])
    assert task.tasks[1] == SingleTask(method="method_2",
                                       args=["arg_3", "arg_4"])

    # dump
    assert task.dump() == config
