# -*- coding: utf-8 -*-


import sys
import time
from pprint import pprint
from io import StringIO

import pytest
import win32con
import win32api
from PIL import Image
from pynput.mouse import Button, Controller

from autogame.base import _all_windows, Window, BaseGame


class Redirect(object):

    def __init__(self):
        self.io_ = StringIO()
        self.file = "log"
        self.need_write = False

    def write(self, f: str = "log"):
        self.file = f
        self.need_write = True

    def to_file(self):
        with open(self.file, "wb") as f:
            f.write(self.io_.getvalue().encode("utf8"))


@pytest.fixture(name="redirector")
def get_redirector():

    r = Redirect()
    r.io_, sys.stdout = sys.stdout, r.io_
    yield r
    r.io_, sys.stdout = sys.stdout, r.io_
    r.to_file()


def test_hh(redirector):
    print(_all_windows())

def test_hehe(redirector):

    name = 'python使用win32后台鼠标点击梦幻西游（只用于开学习技术）新手学习_weixin_47344599的博客-CSDN博客_pywin32 梦幻西游 - Vivaldi'
    w = Window(name)
    print(w.handle)
    long_position = win32api.MAKELONG(20, 30)
    win32api.SendMessage(328950, win32con.WM_LBUTTONDOWN, win32con.MK_LBUTTON, long_position)
    win32api.SendMessage(328950, win32con.WM_LBUTTONUP, win32con.MK_LBUTTON, long_position)

def test_hhh(redirector):

    name = '雷电模拟器'
    game = BaseGame(name)
    # pic = "./pic/myrzx.jpg"
    # pos = game.find_pic(pic)
    # print(pos)
    # game._click(game.find_pic(pic))
    # img = game.capture_window()
    # img = Image.fromarray(img)
    # img.show()
    pos = None
    while True:
        while not game.find_pic("./pic/egg_complete.jpg"):
            time.sleep(60)
        while not game.find_pic("./pic/auto_sell.jpg"):
            pos = game.find_pic("./pic/sell.jpg")
            if pos:
                game._click(pos)
            time.sleep(1)
        while game.find_pic("./pic/auto_sell.jpg"):
            pos = game.find_pic("./pic/yes.jpg")
            if pos:
                game._click(pos)
            time.sleep(1)
        while not game.find_pic("./pic/less_time_first.jpg"):
            pos = game.find_pic("./pic/fill.jpg")
            if pos:
                game._click(pos)
            time.sleep(1)
        while not game.find_pic("./pic/egg_placed.jpg"):
            pos = game.find_pic("./pic/less_time_first.jpg")
            if pos:
                game._click(pos)
            time.sleep(1)

